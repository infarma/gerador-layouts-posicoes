# Gerador de Layouts - Camada de Reflexão - V 0.0.3

#### Razão

Proporcionar uma camada de tradução entre as strings vindas dos arquivos e os atributos de cada layout gerado por meio da ferramenta de [geração de layout](https://bitbucket.org/infarma/gerador-de-layouts/src/master/)

#### Funcionamento

O [gerador-de-layouts](https://bitbucket.org/infarma/gerador-de-layouts/src/master/) cria structs e um hashmap contendo o início, fim e as casas decimais de cada campo.  

Dentro da função ComposeStruct, que existe para cada struct gerado pelo gerador, um objeto PosicaoParaValor é instanciado e recebe a string a ser convertida e o hash contendo as posições.

Após isso, a função ReturnByType() é chamada para cada atributo existente na struct e deve receber a referência do atributo de desitno e o nome desse atributo.

Dentro do pacote [gerador-layouts-posicoes](https://bitbucket.org/infarma/gerador-layouts-posicoes/src/master/), a função ReturnByType() busca no hashmap o nome do atributo passado a fim de obter os valores de início, fim e casas decimais, caso presente.

Utilizando o recurso de type assertion do Go, a função 'descobre' o tipo de destino da string recebida e determina a função de conversão correta a ser utilizada, escrevendo seu resultado na referência (atributo de destino) passada.
 
#### Tipos suportados

- *float32:
- *float64:
- *int32:
- *int64:
- *string:

#### Adicionar suporte a novos tipos

Para adicionar suporte a novos tipos, o switch de type assertion contido na linha 18 do arquivo [posicao_para_valor.go](https://bitbucket.org/infarma/gerador-layouts-posicoes/src/52f3e656665b90d34b0164b1ee33987627d66c63/posicao_para_valor.go#lines-18) deve ser modificado.

Para isso, um novo bloco deve ser adicionado referente ao tipo desejado e uma função que converta tal string para o tipo desejado também deve ser criada. Abaixo está um exemplo:

Caso fosse necessário adicionar suporte ao tipo XYZ: 

````go
case *XYZ:
	interval, err := p.ReturnAsXYZ(key)
	value.Set(reflect.ValueOf(interval))
	return err
````
		
PS: Nesse caso, a função ReturnAsXYZ retornaria um valor já do tipo XYZ. Tal valor seria apenas escrito no atributo de destino através do value.Set()
 
#### Exceções
 
Todos os erros são retornados para a origem do chamado da função e devem ser tratados lá.

 		
#### TODO

* Escrever testes unitários
