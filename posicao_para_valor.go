package gerador_layouts_posicoes

import (
	"errors"
	"fmt"
	"reflect"
)

type PosicaoParaValor struct {
	FileContents string
	Posicoes     map[string]Posicao
}

func (p *PosicaoParaValor) ReturnByType(attribute interface{}, key string) error {
	var err error
	value := reflect.ValueOf(attribute).Elem()

	switch attribute.(type) {
	case *int:
		//	Não utilizado por enquanto
	case *float32:
		interval, err := p.ReturnAsFloat32(key)
		value.Set(reflect.ValueOf(interval))
		return err
	case *float64:
		interval, err := p.ReturnAsFloat64(key)
		value.Set(reflect.ValueOf(interval))
		return err
	case *int32:
		interval, err := p.ReturnAsInt32(key)
		value.Set(reflect.ValueOf(interval))
		return err
	case *int64:
		interval, err := p.ReturnAsInt64(key)
		value.Set(reflect.ValueOf(interval))
		return err
	case *string:
		interval, err := p.ReturnAsString(key)
		value.Set(reflect.ValueOf(interval))
		return err
	default:
		return errors.New(fmt.Sprintf("Erro ao converter tipo para %s", key))
	}

	return err
}

func (p *PosicaoParaValor) ReturnAsFloat32(key string) (float32, error) {
	valueAsString, decimalPlaces, err := p.ReturnContentWithDecimalPlace(key)
	value, err := StringToFloat64(valueAsString, decimalPlaces)

	return float32(value), err
}

func (p *PosicaoParaValor) ReturnAsFloat64(key string) (float64, error) {
	valueAsString, decimalPlaces, err := p.ReturnContentWithDecimalPlace(key)
	value, err := StringToFloat64(valueAsString, decimalPlaces)

	return value, err
}

func (p *PosicaoParaValor) ReturnContent(key string) (string, error) {
	start := p.Posicoes[key].Inicio
	end := p.Posicoes[key].Fim

	return p.FileContents[start:end], nil
}

func (p *PosicaoParaValor) ReturnContentWithDecimalPlace(key string) (string, int, error) {
	start := p.Posicoes[key].Inicio
	end := p.Posicoes[key].Fim
	decimalPlaces := p.Posicoes[key].CasasDecimais

	return p.FileContents[start:end], decimalPlaces, nil
}

func (p *PosicaoParaValor) ReturnAsInt32(key string) (int32, error) {
	value, err := p.ReturnAsInt64(key)

	return int32(value), err
}

func (p *PosicaoParaValor) ReturnAsInt64(key string) (int64, error) {
	valueAsString, err := p.ReturnContent(key)
	value, err := StringToInt64(valueAsString)

	return value, err
}

func (p *PosicaoParaValor) ReturnAsString(key string) (string, error) {
	return p.ReturnContent(key)
}
