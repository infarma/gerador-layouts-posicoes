package gerador_layouts_posicoes

import (
	"cloud.google.com/go/civil"
	"strconv"
	"time"
)

func StringToFloat64(text string, decimalPlaces int) (float64, error) {
	some := text[:len(text)-decimalPlaces]+"."+text[len(text)-decimalPlaces:]
	value, err := strconv.ParseFloat(some, 10)
	return value, err
}

func StringToInt64(text string) (int64, error) {
	value, err := strconv.ParseInt(text, 10, 64)
	return value, err
}

func StringToInt(text string) (int, error) {
	value, err := StringToInt64(text)
	return int(value), err
}

func StringToCivilDate(fileContents string) (civil.Date, error) {
	return civil.ParseDate(fileContents)
}

func StringToTimeTime(fileContents string) (time.Time, error) {
	return time.Parse("20060102", fileContents)
}